package com.pedroribeiro.casterioroomcourse

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.pedroribeiro.casterioroomcourse.task.Task
import com.pedroribeiro.casterioroomcourse.task.TaskDao
import com.pedroribeiro.casterioroomcourse.user.User
import com.pedroribeiro.casterioroomcourse.user.UserDao

/**
 * Created by Pedro Ribeiro on 19/11/2018.
 */
@Database(
    version = 1, entities = [
        Task::class,
        User::class
    ]
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null
        private val DATABASE_NAME = "app-database"

        fun getInstance(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }
        }


        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }

}