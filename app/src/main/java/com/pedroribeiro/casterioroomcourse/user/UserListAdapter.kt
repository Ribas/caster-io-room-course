package com.pedroribeiro.casterioroomcourse.user

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pedroribeiro.casterioroomcourse.R
import kotlinx.android.synthetic.main.item_user_row.view.*
import kotlin.concurrent.thread

class UserListAdapter(private val userDao: UserDao) :
    ListAdapter<UserDao.UserAndTasks, UserListAdapter.UserListViewHolder>(DIFF_UTIL_CALLBACK) {

    companion object {
        val DIFF_UTIL_CALLBACK = object : DiffUtil.ItemCallback<UserDao.UserAndTasks>() {
            override fun areItemsTheSame(oldItem: UserDao.UserAndTasks, newItem: UserDao.UserAndTasks): Boolean {
                return oldItem.user.id == newItem.user.id
            }

            override fun areContentsTheSame(p0: UserDao.UserAndTasks, p1: UserDao.UserAndTasks): Boolean {
                return p0 == p1
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): UserListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return UserListViewHolder(
            inflater.inflate(
                R.layout.item_user_row,
                parent,
                false
            ), userDao
        )
    }

    override fun onBindViewHolder(holder: UserListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class UserListViewHolder(private val view: View, private val userDao: UserDao) : RecyclerView.ViewHolder(view) {
        fun bind(userAndTasks: UserDao.UserAndTasks) {
            itemView.userName.text = userAndTasks.user.name
            itemView.textView.text = view.context.getString(R.string.user_number_of_tasks, userAndTasks.tasks.size)
            itemView.deleteUser.setOnClickListener {
                thread { userDao.delete(userAndTasks.user) }
            }
        }
    }

}
