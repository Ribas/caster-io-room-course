package com.pedroribeiro.casterioroomcourse.user

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.pedroribeiro.casterioroomcourse.task.Task

/**
 * Created by Pedro Ribeiro on 22/11/2018.
 */
@Dao
interface UserDao{

    @Insert
    fun insert(user: User): Long

    @Delete
    fun delete(user: User)

    @Query("SELECT * From User")
    fun getUsers(): LiveData<List<User>>

    @Transaction
    @Query("SELECT * FROM User")
    fun getAllUsersAndTasks(): LiveData<List<UserAndTasks>>

    class UserAndTasks {
        @Embedded lateinit var user: User
        @Relation(parentColumn = "id", entityColumn = "userId") lateinit var tasks: List<Task>
    }

}