package com.pedroribeiro.casterioroomcourse.user

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Pedro Ribeiro on 22/11/2018.
 */
@Entity
data class User(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    var name: String
)