package com.pedroribeiro.casterioroomcourse.user

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.pedroribeiro.casterioroomcourse.AppDatabase
import com.pedroribeiro.casterioroomcourse.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_user.*
import kotlin.concurrent.thread

/**
 * Created by Pedro Ribeiro on 21/11/2018.
 */
class UserActivity : AppCompatActivity() {

    private val userDao by lazy { AppDatabase.getInstance(this).userDao() }

    private lateinit var userListAdapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        setSupportActionBar(toolbar)
        toolbar.title = "Users"
        addUserButton.setOnClickListener {
            addUser()
            userNameText.setText("")
        }

        userListAdapter = UserListAdapter(userDao)
        userList.layoutManager = LinearLayoutManager(this)
        userList.adapter = userListAdapter
        userDao.getAllUsersAndTasks().observe(this, Observer<List<UserDao.UserAndTasks>> {
            userListAdapter.submitList(it)
        })
    }

    private fun addUser() {
        val userName = userNameText.text.toString()
        if (userName.isBlank()) {
            Snackbar.make(toolbar, "Task title is required", Snackbar.LENGTH_SHORT).show()
            return
        }

        val user = User(name = userName)

        thread {
            userDao.insert(user)
        }
    }

}