package com.pedroribeiro.casterioroomcourse.task

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.pedroribeiro.casterioroomcourse.user.User

/**
 * Created by Pedro Ribeiro on 19/11/2018.
 */
@Dao
interface TaskDao {

    @Insert
    fun insert(task: Task): Long

    @Insert
    fun insertAll(task: List<Task>): List<Long>

    @Query("SELECT * FROM Task")
    fun getAllTasks(): LiveData<List<Task>>

    @Query("SELECT * FROM Task WHERE id = :taskId")
    fun getTask(taskId: Int): LiveData<Task>

    @Query("SELECT Task.* FROM Task LEFT OUTER JOIN User ON Task.userId = User.id")
    fun getTasksAndUsers(): LiveData<List<Task>>

    @Query("SELECT Task.*, User.id as user_id, User.name as user_name FROM Task LEFT OUTER JOIN User ON Task.userId == User.id WHERE Task.id = :taskId")
    fun getTaskAndUser(taskId: Int): LiveData<UserTask>

    @Update
    fun update(task: Task)

    @Delete
    fun delete(task: Task)

    data class UserTask(
        @Embedded(prefix = "user_") var user: User?,
        @Embedded var task: Task?
    )

}