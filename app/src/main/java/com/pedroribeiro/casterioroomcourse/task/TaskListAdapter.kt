package com.pedroribeiro.casterioroomcourse.task

import android.content.Context
import android.content.Intent
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pedroribeiro.casterioroomcourse.R
import kotlinx.android.synthetic.main.item_task_row.view.*
import kotlin.concurrent.thread

class TaskListAdapter(private val taskDao: TaskDao) : ListAdapter<Task, TaskListAdapter.TaskListViewHolder>(
    DIFF_UTIL_CALLBACK
) {

    companion object {

        val DIFF_UTIL_CALLBACK = object : DiffUtil.ItemCallback<Task>() {
            override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(p0: Task, p1: Task): Boolean {
                return p0 == p1
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TaskListViewHolder(
            inflater.inflate(
                R.layout.item_task_row,
                parent,
                false

            ), taskDao
        )
    }

    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class TaskListViewHolder(val view: View, val taskDao: TaskDao) : RecyclerView.ViewHolder(view) {
        fun bind(task: Task) {
            itemView.taskTitle.text = task.title
            itemView.taskCompleted.text = if (task.completed) "Completed" else "Not Completed"
            itemView.setOnClickListener {
                navigateToTaskActivity(view.context, task)
            }
            itemView.deleteTask.setOnClickListener {
                thread { taskDao.delete(task) }
            }
        }

        private fun navigateToTaskActivity(
            context: Context,
            task: Task
        ) {
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra("Task Id", task.id)
            view.context.startActivity(intent)
        }
    }

}
