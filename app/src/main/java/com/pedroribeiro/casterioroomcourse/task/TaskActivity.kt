package com.pedroribeiro.casterioroomcourse.task

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.annotation.AnyThread
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.pedroribeiro.casterioroomcourse.AppDatabase
import com.pedroribeiro.casterioroomcourse.R
import com.pedroribeiro.casterioroomcourse.user.User
import com.pedroribeiro.casterioroomcourse.user.UserDao
import kotlinx.android.synthetic.main.activity_task.*
import kotlin.concurrent.thread

/**
 * Created by Pedro Ribeiro on 20/11/2018.
 */
class TaskActivity : AppCompatActivity() {

    private lateinit var taskDao: TaskDao
    private lateinit var userDao: UserDao
    private lateinit var assigneeArrayAdapter: ArrayAdapter<UserSelectionChoice>
    private var spinnerInitialised = false
    private var task: Task? = null //might not be initialized when checkbox oncheckedchangelistener is triggered

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        configureAssigneeAdapter()
        taskDao = AppDatabase.getInstance(this).taskDao()
        userDao = AppDatabase.getInstance(this).userDao()
        userDao.getUsers().observe(this, Observer<List<User>> { it ->
            it?.forEach {
                assigneeArrayAdapter.add(UserSelectionChoice.SelectedUser(it))
                assigneeArrayAdapter.add(UserSelectionChoice.Unassign)
                assignee.isEnabled = true
            }
        })
        taskDao.getTaskAndUser(getTaskIdFromIntent() ?: Int.MIN_VALUE).observe(this, Observer<TaskDao.UserTask> { it ->
            if (it == null) {
                finish()
                return@Observer
            }
            task = it.task
            updateUserDetails(it.user)
            updateTaskDetails(it.task)
        })
        checkbox.setOnCheckedChangeListener { _, isChecked ->
            task?.let {
                it.completed = isChecked
                updateTask(it)
            }
        }
    }

    private fun updateTaskDetails(task: Task?) {
        if (task != null) {
            task_Id.text = task.id.toString()
            checkbox.isChecked = task.completed
        }
    }

    private fun updateUserDetails(user: User?) {
        assignedUserId.text = user?.name ?: getString(R.string.unassigned)
    }

    @AnyThread
    private fun updateTask(task: Task) {
        thread { taskDao.update(task) }
    }

    private fun getTaskIdFromIntent() = intent?.extras?.getInt("Task Id")

    private fun configureAssigneeAdapter() {
        assigneeArrayAdapter = AssigneeAdapter(this, android.R.layout.simple_spinner_item)
        assigneeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        assigneeArrayAdapter.add(UserSelectionChoice.Instruction)
        assignee.adapter = assigneeArrayAdapter
        assignee.isEnabled = false

        assignee.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                // ignore the first event - UI initialising causes this and not a user selection
                if (!spinnerInitialised) {
                    spinnerInitialised = true
                    return
                }

                val selectedUserChoice = assigneeArrayAdapter.getItem(position)
                when (selectedUserChoice) {
                    is UserSelectionChoice.SelectedUser -> assignUserToTask(selectedUserChoice.user)
                    UserSelectionChoice.Unassign -> unassignUserFromTask()
                }

                // reset to show "assign task" instruction
                assignee.setSelection(0, true)
            }
        }
    }

    private fun assignUserToTask(user: User) {
        task?.let {
            it.userId = user.id
            updateTask(it)
        }
    }

    private fun unassignUserFromTask() {
        task?.let {
            it.userId = null
            updateTask(it)
        }
    }

    private sealed class UserSelectionChoice {
        object Instruction : UserSelectionChoice()
        object Unassign : UserSelectionChoice()
        data class SelectedUser(val user: User) : UserSelectionChoice()
    }

    private class AssigneeAdapter(context: Context, resource: Int) :
        ArrayAdapter<UserSelectionChoice>(context, resource) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            return configureListView(position, convertView, parent)
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            return configureListView(position, convertView, parent)
        }

        private fun configureListView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val listItemView: View = convertView ?: LayoutInflater.from(context).inflate(
                android.R.layout.simple_spinner_dropdown_item,
                parent,
                false
            )
            val userSelectionChoice = getItem(position)

            val textView = listItemView.findViewById<View>(android.R.id.text1) as TextView

            textView.text = when (userSelectionChoice) {
                is UserSelectionChoice.SelectedUser -> {
                    "${userSelectionChoice.user.name} (id = ${userSelectionChoice.user.id})"
                }
                UserSelectionChoice.Instruction -> "Assign Task"
                UserSelectionChoice.Unassign -> "Unassign"
            }

            return listItemView
        }
    }

}