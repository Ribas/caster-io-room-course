package com.pedroribeiro.casterioroomcourse

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import com.pedroribeiro.casterioroomcourse.task.Task
import com.pedroribeiro.casterioroomcourse.task.TaskDao
import com.pedroribeiro.casterioroomcourse.task.TaskListAdapter
import com.pedroribeiro.casterioroomcourse.user.UserActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private lateinit var database: AppDatabase
    private lateinit var taskListAdapter: TaskListAdapter
    private lateinit var taskDao: TaskDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        database = AppDatabase.getInstance(this)
        taskDao = database.taskDao()

        addTaskButton.setOnClickListener {
            addTask()
            edit_text.setText("")
        }

        edit_text.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == IME_ACTION_DONE) {
                addTask()
                edit_text.setText("")
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        taskListAdapter = TaskListAdapter(taskDao)
        taskList.layoutManager = LinearLayoutManager(this)
        taskList.adapter = taskListAdapter
        taskDao.getAllTasks().observe(this, Observer<List<Task>> {
            taskListAdapter.submitList(it)
        })
    }

    private fun addTask() {
        val title = edit_text.text.toString()
        if (title.isBlank()) {
            Snackbar.make(toolbar, "Task title is required", Snackbar.LENGTH_SHORT).show()
            return
        }

        val task = Task(title = title)
        thread {
            taskDao.insert(task)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.manageUsers -> {
                navigateToUserActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun navigateToUserActivity() {
        val intent = Intent(this, UserActivity::class.java)
        startActivity(intent)
    }

}